function changeColorToRed(){
    
    let p = document.querySelector("p");
    p.setAttribute("class", "red")
}

function changeColorToGreen(){
    
    let p = document.querySelector("p");
    p.setAttribute("class", "green")
}

function changeColorToBlue(){
    
    let p = document.querySelector("p");
    p.setAttribute("class", "blue")
}

function changeSizeToSmall(){
    let img = document.querySelector("img");
    img.setAttribute("class", "small")
}

function changeSizeToLarge(){
    let img = document.querySelector("img");
    img.setAttribute("class", "large")
}

function changeColorToBlank(){
    let img = document.querySelector("img");
    img.setAttribute("class", "black")
}

function changeColorToColor(){
    let img = document.querySelector("img");
    img.setAttribute("class", "color")
}

function changeDirectionLeft(){
    let img = document.querySelector("img");
    img.setAttribute("class", "kaire")
}

function changeDirectionRight(){
    let img = document.querySelector("img");
    img.setAttribute("class", "desine")
}

function changeDirectionBegin(){
    let img = document.querySelector("img");
    img.setAttribute("class", "atstatyti")
}

function changeDirectionDown(){
    let img = document.querySelector("img");
    img.setAttribute("class", "apacia")
}

function changeDirectionNone(){
    let img = document.querySelector("img");
    img.setAttribute("class", "nebera")
}