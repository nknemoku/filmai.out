let form = document.querySelector("form");
var bandymuSkaicius = 1;
if (sugeneruotasSkaicius === undefined) {
    var sugeneruotasSkaicius = Math.floor((Math.random() * 10) + 1);
}
form.addEventListener("submit", function(e) {
    e.preventDefault();
    console.log(e.target.elements.skaicius.value);
    if (isNaN(e.target.elements.skaicius.value)) {
        alert("Įvestas skaičius neatitinka");
    }
    if (sugeneruotasSkaicius > e.target.elements.skaicius.value) {
        alert("Įvestas skaičius mažesnis, nei sugeneruotas skaičius");
    }
    if (sugeneruotasSkaicius < e.target.elements.skaicius.value) {
        alert("Įvestas skaičius didesnis, nei sugeneruotas skaičius");
    }
    if (sugeneruotasSkaicius == e.target.elements.skaicius.value) {
        alert("Sveikiname, laimėjote, prireikė bandymų: " + bandymuSkaicius);
        window.location = 'index.html';
    }
    bandymuSkaicius++;
})

function pranesimas() {
    var a = document.forms["form"]["skaicius"].value;
    document.getElementById("ivestSkaicius").innerHTML = a;
    document.getElementById("sugSkaicius").innerHTML = sugeneruotasSkaicius;
    document.getElementById("bandymai").innerHTML = bandymuSkaicius;
}