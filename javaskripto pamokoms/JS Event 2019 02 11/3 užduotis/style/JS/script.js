let form = document.querySelector("form");
form.addEventListener("submit", function(e) {
    e.preventDefault();
    if (isNaN(e.target.elements.mase.value) || isNaN(e.target.elements.ugis.value)) {
        alert("Įvesti skaičiai neatitinka skaičių apibrėžimo");
    }
    if (e.target.elements.mase.value>500 || e.target.elements.ugis.value*100>250) {
        alert("500kg ir 250 centimetru yra maksimumas");
    }
    if (e.target.elements.mase.value<5 || e.target.elements.ugis.value*100<20) {
        alert("5kg ir 20 centimetru yra minimumas");
    }
})


function pranesimas() {
    var rezultatas = +document.forms["form"]["mase"].value / (+document.forms["form"]["ugis"].value * +document.forms["form"]["ugis"].value); 
    document.getElementById("ivestSkaicius").innerHTML = rezultatas;
}