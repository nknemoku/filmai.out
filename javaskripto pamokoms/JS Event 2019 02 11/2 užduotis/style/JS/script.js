let form = document.querySelector("form");
form.addEventListener("submit", function(e) {
    e.preventDefault();
    if (isNaN(e.target.elements.pirmas.value) || isNaN(e.target.elements.antras.value)) {
        alert("Įvesti skaičiai neatitinka skaičių apibrėžimo");
    }
})

function pranesimas() {
    var rezultatas = +document.forms["form"]["pirmas"].value + +document.forms["form"]["antras"].value; 
    document.getElementById("ivestSkaicius").innerHTML = rezultatas;
}