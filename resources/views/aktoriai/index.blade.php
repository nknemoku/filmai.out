@extends('layout.main')
 
@section('content')
    <h1>Aktoriai</h1>
    @if(count($aktoriai) > 0)
        @foreach($aktoriai as $aktorius)
            <div class="well">
                <h3><a href="/filmai/public/aktoriai/{{$aktorius->id}}">{{$aktorius->vardas}}  {{$aktorius->pavarde}}</a></h3>
            </div>
        @endforeach
        {{$aktoriai->links()}}
    @else
        <p>Nera jokiu aktoriu</p>
    @endif
    <a class='btn btn-primary' href="{{ route('aktoriai.create') }}">Pridėti aktorių</a>
@endsection