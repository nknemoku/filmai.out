@extends('layout.main')
 
@section('content')
    <h1>{{$aktorius->vardas}} {{$aktorius->pavarde}}</h1>
    <a href="{{ route('aktoriai.edit', $aktorius->id)}}" class="btn btn-sm btn-primary">Redaguoti</a>
    <form action="{{ route('aktoriai.destroy', $aktorius->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger btn-sm" type="submit">Trinti</button>
    </form>
@endsection