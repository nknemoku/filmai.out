@extends('layout.main')
 
@section('content')
<!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg1.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Filmų aprašymai</h1>
            <span class="subheading">Skaitykite naujausių filmų aprašymus</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto-4">
        <div class="deze">
        <div class="post-preview">
          <a href="post.html">
            <img class="post-title" src="img/virselis1.jpg">
            <h2 class="post-title">
              Filmo pavadinimas
            </h2>
            <h3 class="post-subtitle">
              Filmo aprašymas
            </h3>
          </a>
          <p class="post-meta">Pildė:
            <a href="#">Vardas Pavardė</a>
            Vasario 12, 2019</p>
        </div>
        </div>
        <hr>
        <!-- Pager -->
        <div class="clearfix">
          <a class="btn btn-primary float-right" href="#">Senesni įrašai &rarr;</a>
        </div>
      </div>
    </div>
  </div>

  <hr>
@endsection