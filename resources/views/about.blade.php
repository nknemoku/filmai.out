@extends('layout.main')
 
@section('content')
<!-- Page Header -->
  <header class="masthead" style="background-image: url('img/about-bg1.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Apie svetaine</h1>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p>Filmai.out – Lietuvos kino naujienų tinklalapis. Esame entuziastinga rašančių ir fotografuojančių žmonių komanda,
        rengianti interviu su Lietuvos kino kūrėjais, reportažus iš kino renginių ir premjerų. Puslapyje taip pat talpinama
        įvairi ir aktuali informacija apie kino filmus, kino industrijos įvykius ir renginius.</p>
      </div>
    </div>
  </div>

  <hr>
@endsection