@extends('layout.main')
 
@section('content')
    <h1>Sukurti rezisieriu</h1>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    {!! Form::open(array('route' => 'rezisieriai.store')) !!}
        {!! Form::token() !!}
        <div class="form-group">
            {!! Form::label('vardas', 'Vardas') !!}
            {!! Form::text('vardas', ' ', array('class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            {!! Form::label('pavarde', 'Pavardė') !!}
            {!! Form::text('pavarde', ' ', array('class' => 'form-control')) !!}
        </div>
        {!! Form::submit('Pridėti', array('class' => 'btn btn-primary')) !!} 
    {!! Form::close() !!}
@endsection