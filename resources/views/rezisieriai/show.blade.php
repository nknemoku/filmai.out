@extends('layout.main')
 
@section('content')
    <h1>{{$rezisierius->vardas}} {{$rezisierius->pavarde}}</h1>
    <a href="{{ route('rezisieriai.edit', $rezisierius->id)}}" class="btn btn-sm btn-primary">Redaguoti</a>
    <form action="{{ route('rezisieriai.destroy', $rezisierius->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger btn-sm" type="submit">Trinti</button>
    </form>
@endsection