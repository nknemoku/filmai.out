@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Redaguoti darbą
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    {!! Form::open(array('route' => array('rezisieriai.update', $rezisierius->id))) !!}
        @method('PATCH')
        {!! Form::token() !!}
        {!! Form::hidden('id', $rezisierius->id) !!}
        <div class="form-group">
            {!! Form::label('vardas', 'Vardas') !!}
            {!! Form::text('vardas', $rezisierius->vardas, array('class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            {!! Form::label('pavarde', 'Pavardė') !!}
            {!! Form::text('pavarde', $rezisierius->pavarde, array('class' => 'form-control')) !!}
        </div>
        {!! Form::submit('Pridėti', array('class' => 'btn btn-primary')) !!} 
    {{ Form::close() }}
  </div>
</div>
@endsection