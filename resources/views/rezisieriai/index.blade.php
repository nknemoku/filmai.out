@extends('layout.main')
 
@section('content')
    <h1>Rezisieriai</h1>
    @if(count($rezisieriai) > 0)
        @foreach($rezisieriai as $rezisierius)
            <div class="well">
                <h3><a href="/filmai/public/rezisieriai/{{$rezisierius->id}}">{{$rezisierius->vardas}}  {{$rezisierius->pavarde}}</a></h3>
            </div>
        @endforeach
        {{$rezisieriai->links()}}
    @else
        <p>Nera jokiu rezisieriu</p>
    @endif
    <a class='btn btn-primary' href="{{ route('rezisieriai.create') }}">Pridėti režisieriu</a>
@endsection