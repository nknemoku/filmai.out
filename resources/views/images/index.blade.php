@extends('layout.main')
 
@section('content')
    <h1>Paveikslėliai</h1>
    @if(count($images) > 0)
        @foreach($images as $image)
            <div class="well">
                <h3><a href="/filmai/public/paveiksleliai/{{$image->id}}">{{$image->pavadinimas}}</a></h3>
                <div><img src="{{ asset('images/') }}/{{$image->kelias}}" alt="{{$image->pavadinimas}}"></div>
            </div>
        @endforeach
        {{$images->links()}}
    @else
        <p>Nėra jokiu paveikslėlių</p>
    @endif
    <a class='btn btn-primary' href="{{ route('paveiksleliai.create') }}">Įkelti paveikslėlį</a>
@endsection