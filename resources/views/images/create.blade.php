@extends('layout.main')
 
@section('content')
    <h1>Įkelti paveikslėlį</h1>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    {!! Form::open(array('route' => 'paveiksleliai.store', 'files'=>true)) !!}
        {!! Form::token() !!}
        <div class="form-group">
            {!! Form::label('pavadinimas', 'Pavadinimas') !!}
            {!! Form::text('pavadinimas', ' ', array('class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            {!! Form::label('kelias', 'Įkelti paveikslėlį') !!}
            {!! Form::file('kelias', array('class' => 'form-control')) !!}
        </div>
        {!! Form::submit('Pridėti', array('class' => 'btn btn-primary')) !!} 
    {!! Form::close() !!}
@endsection