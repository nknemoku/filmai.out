@extends('layout.main')
 
@section('content')
<h3><a href="/filmai/public/paveiksleliai/{{$image->id}}">{{$image->pavadinimas}}</a></h3>
<div><img src="{{ asset('images/') }}/{{$image->kelias}}" alt="{{$image->pavadinimas}}"></div>
    <a href="{{ route('paveiksleliai.edit', $image->id)}}" class="btn btn-sm btn-primary">Redaguoti</a>
    <form action="{{ route('paveiksleliai.destroy', $image->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger btn-sm" type="submit">Trinti</button>
    </form>
@endsection