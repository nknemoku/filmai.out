@extends('layout.main')
 
@section('content')
    <h1>{{$zanras->vardas}} {{$zanras->pavarde}}</h1>
    <a href="{{ route('zanrai.edit', $zanras->id)}}" class="btn btn-sm btn-primary">Redaguoti</a>
    <form action="{{ route('zanrai.destroy', $zanras->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger btn-sm" type="submit">Trinti</button>
    </form>
@endsection