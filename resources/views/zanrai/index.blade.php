@extends('layout.main')
 
@section('content')
    <h1>Žanrai</h1>
    @if(count($zanrai) > 0)
        @foreach($zanrai as $zanras)
            <div class="well">
                <h3><a href="/filmai/public/zanrai/{{$zanras->id}}">{{$zanras->pavadinimas}}</a></h3>
            </div>
        @endforeach
        {{$zanrai->links()}}
    @else
        <p>Nera jokiu žanrų</p>
    @endif
    <a class='btn btn-primary' href="{{ route('zanrai.create') }}">Pridėti žanrą</a>
@endsection