@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Redaguoti žanrą
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    {!! Form::open(array('route' => array('zanrai.update', $zanras->id))) !!}
        @method('PATCH')
        {!! Form::token() !!}
        {!! Form::hidden('id', $zanras->id) !!}
        <div class="form-group">
            {!! Form::label('pavadinimas', 'Pavadinimas') !!}
            {!! Form::text('pavadinimas', $zanras->pavadinimas, array('class' => 'form-control')) !!}
        </div>
        {!! Form::submit('Pridėti', array('class' => 'btn btn-primary')) !!} 
    {{ Form::close() }}
  </div>
</div>
@endsection