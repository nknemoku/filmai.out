@extends('layout.main')
 
@section('content')
    <h1>Sukurti žanrą</h1>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    {!! Form::open(array('route' => 'zanrai.store')) !!}
        {!! Form::token() !!}
        <div class="form-group">
            {!! Form::label('pavadinimas', 'Pavadinimas') !!}
            {!! Form::text('pavadinimas', ' ', array('class' => 'form-control')) !!}
        </div>
        {!! Form::submit('Pridėti', array('class' => 'btn btn-primary')) !!} 
    {!! Form::close() !!}
@endsection