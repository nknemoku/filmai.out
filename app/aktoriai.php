<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aktoriai extends Model
{
    protected $fillable = [
        'id',
        'vardas',
        'pavarde'
    ];
    protected $table = 'aktoriai';
    public $primaryKey = 'id';
    public $timestamps = false;
}
