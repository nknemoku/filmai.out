<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paveiksleliai extends Model
{
    protected $fillable = [
        'id',
        'pavadinimas',
        'kelias'
    ];
    protected $table = 'paveiksleliai';
    public $primaryKey = 'id';
    public $timestamps = false;
}
