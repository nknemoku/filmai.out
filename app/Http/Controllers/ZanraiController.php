<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\zanrai;

class ZanraiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $zanras = zanrai::orderBy('pavadinimas')->paginate(15);

        return view('zanrai.index')->with('zanrai', $zanras);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('zanrai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            // The user is logged in...
            $id = Auth::id();
            $request->validate([
                'pavadinimas'=>'required',
            ]);
            $zanras = new zanrai([
                'pavadinimas' => $request->get('pavadinimas')
            ]);
            $zanras->save();
            return redirect('/zanrai')->with('success', 'Žanras buvo sėkmingai sukurtas');
        } else {
            return redirect('/')->with('error', 'Būtina prisijungti');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $zanras = zanrai::find($id);
        return view('zanrai.show')->with('zanras', $zanras);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $zanras = zanrai::find($id);

        return view('zanrai.edit', compact('zanras'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            // The user is logged in...
            $request->validate([
                'pavadinimas'=>'required'
            ]);

            $id = $request->get('id');

            $zanras = zanrai::find($id);
            $zanras->pavadinimas = $request->get('pavadinimas');
            $zanras->save();

            return redirect('/zanrai')->with('success', 'Žanras buvo sėkmingai atnaujintas');
        } else {
            return redirect('/')->with('error', 'Būtina prisijungti');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $zanras = zanrai::find($id);
        $zanras->delete();

        return redirect('/zanrai')->with('success', 'Žanras sėkmingai pašalintas');
    }
}
