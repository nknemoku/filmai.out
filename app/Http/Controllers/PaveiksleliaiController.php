<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\paveiksleliai;

class PaveiksleliaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = paveiksleliai::orderBy('pavadinimas')->paginate(15);

        return view('images.index')->with('images', $images);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            // The user is logged in...
            $request->validate([
                'pavadinimas'=>'required',
                 'kelias' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]);
           
            $imageName = time().'.'.request()->kelias->getClientOriginalExtension();
            request()->kelias->move(public_path('images'), $imageName); 
            
            $paveiksleliai = new paveiksleliai([
                'pavadinimas' => $request->get('pavadinimas'),
                'kelias'  => $imageName
            ]);   

            $paveiksleliai->save();
            return redirect('/paveiksleliai')->with('success', 'Paveikslėlis buvo įkeltas');
        } else {
            return redirect('/')->with('error', 'Būtina prisijungti');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paveiksleliai = paveiksleliai::find($id);
        return view('images.show')->with('images', $paveiksleliai);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paveiksleliai = paveiksleliai::find($id);

        return view('images.edit', compact('paveiksleliai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            // The user is logged in...
            $request->validate([
                'pavadinimas'=>'required'
            ]);

            $id = $request->get('id');

            $zanras = zanrai::find($id);
            $zanras->pavadinimas = $request->get('pavadinimas');
            $zanras->save();

            return redirect('/zanrai')->with('success', 'Žanras buvo sėkmingai atnaujintas');
        } else {
            return redirect('/')->with('error', 'Būtina prisijungti');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paveiksleliai = paveiksleliai::find($id);
        Storage::delete($imageName->kelias);

        $paveiksleliai->delete();

        return redirect('/zanrai')->with('success', 'Žanras sėkmingai pašalintas');
    }
}
