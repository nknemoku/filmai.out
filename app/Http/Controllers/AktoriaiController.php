<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\aktoriai;

class AktoriaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aktorius = aktoriai::orderBy('vardas')->paginate(15);

        return view('aktoriai.index')->with('aktoriai', $aktorius);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aktoriai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            // The user is logged in...
            $id = Auth::id();
            $request->validate([
                'vardas'=>'required',
                'pavarde'=>'required'
            ]);
            $aktorius = new aktoriai([
                'vardas' => $request->get('vardas'),
                'pavarde' => $request->get('pavarde')
            ]);
            $aktorius->save();
            return redirect('/aktoriai')->with('success', 'Aktorius buvo sėkmingai sukurtas');
        } else {
            return redirect('/')->with('error', 'Būtina prisijungti');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aktorius = aktoriai::find($id);
        return view('aktoriai.show')->with('aktorius', $aktorius);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aktorius = aktoriai::find($id);

        return view('aktoriai.edit', compact('aktorius'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            // The user is logged in...
            $request->validate([
                'vardas'=>'required',
                'pavarde'=>'required'
            ]);

            $id = $request->get('id');

            $aktorius = aktoriai::find($id);
            $aktorius->vardas = $request->get('vardas');
            $aktorius->pavarde = $request->get('pavarde');
            $aktorius->save();

            return redirect('/aktoriai')->with('success', 'Aktorius buvo sėkmingai atnaujintas');
        } else {
            return redirect('/')->with('error', 'Būtina prisijungti');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aktorius = aktoriai::find($id);
        $aktorius->delete();

        return redirect('/aktoriai')->with('success', 'Aktorius sėkmingai pašalintas');
    }
}
