<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\rezisieriai;

class RezisieriaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rezisierius = rezisieriai::orderBy('vardas')->paginate(15);

        return view('rezisieriai.index')->with('rezisieriai', $rezisierius);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rezisieriai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            // The user is logged in...
            $id = Auth::id();
            $request->validate([
                'vardas'=>'required',
                'pavarde'=>'required'
            ]);
            $rezisierius = new rezisieriai([
                'vardas' => $request->get('vardas'),
                'pavarde' => $request->get('pavarde')
            ]);
            $rezisierius->save();
            return redirect('/rezisieriai')->with('success', 'Režisierius buvo sėkmingai sukurtas');
        } else {
            return redirect('/')->with('error', 'Būtina prisijungti');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rezisierius = rezisieriai::find($id);
        return view('rezisieriai.show')->with('rezisierius', $rezisierius);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rezisierius = rezisieriai::find($id);

        return view('rezisieriai.edit', compact('rezisierius'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            // The user is logged in...
            $request->validate([
                'vardas'=>'required',
                'pavarde'=>'required'
            ]);

            $id = $request->get('id');

            $rezisierius = rezisieriai::find($id);
            $rezisierius->vardas = $request->get('vardas');
            $rezisierius->pavarde = $request->get('pavarde');
            $rezisierius->save();

            return redirect('/rezisieriai')->with('success', 'Režisierius buvo sėkmingai atnaujintas');
        } else {
            return redirect('/')->with('error', 'Būtina prisijungti');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rezisierius = rezisieriai::find($id);
        $rezisierius->delete();

        return redirect('/rezisieriai')->with('success', 'Režisierius sėkmingai pašalintas');
    }
}
