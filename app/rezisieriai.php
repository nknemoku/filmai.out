<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rezisieriai extends Model
{
    protected $fillable = [
        'id',
        'vardas',
        'pavarde'
    ];
    protected $table = 'rezisieriai';
    public $primaryKey = 'id';
    public $timestamps = false;
}
