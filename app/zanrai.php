<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class zanrai extends Model
{
    protected $fillable = [
        'id',
        'pavadinimas'
    ];
    protected $table = 'zanrai';
    public $primaryKey = 'id';
    public $timestamps = false;
}
