<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "FilmaiController@index");
Route::get('/about', "FilmaiController@about");
Route::get('/contact', "FilmaiController@contact");
Route::get('/movies', "FilmaiController@movies");

Auth::routes();

Route::resource('aktoriai', 'AktoriaiController');
Route::resource('rezisieriai', 'RezisieriaiController');
Route::resource('zanrai', 'ZanraiController');
Route::resource('paveiksleliai', 'PaveiksleliaiController');

Route::get('/home', 'HomeController@index')->name('home');
/*
|--------------------------------------------------------------------------
| These routes were added by jamesmills/laravel-admin page
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'roles'], 'roles' => 'admin'], function () {
    Route::get('/', 'Admin\AdminController@index')->name('dashboard');
    Route::resource('roles', '\JamesMills\LaravelAdmin\Controllers\Admin\RolesController');
    Route::resource('permissions', '\JamesMills\LaravelAdmin\Controllers\Admin\PermissionsController');
    Route::resource('users', '\JamesMills\LaravelAdmin\Controllers\Admin\UsersController');
});
