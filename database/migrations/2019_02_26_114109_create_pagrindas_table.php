<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagrindasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()

    {	
        
        Schema::create('paveiksleliai', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('pavadinimas');
            $table->string('kelias');
        });
        
        Schema::create('galerija', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('pavadinimas');
            $table->string('nuoroda');
            $table->string('tipas');
            $table->integer('nr')->unsigned();
            $table->integer('pavID')->nullable()->unsigned();
            $table->foreign('pavID')->references('id')->on('paveiksleliai');
        });
		
		  Schema::create('aktoriai', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('vardas');
            $table->mediumText('pavarde');
        });
		
		  Schema::create('rezisieriai', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('vardas');
            $table->mediumText('pavarde');
        });
		
		  Schema::create('zanrai', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('pavadinimas');
        });
		
        Schema::create('filmai', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('pavadinimas');
            $table->date('metai');
            $table->text('aprasymas');
            $table->integer('pavID')->nullable()->unsigned();
            $table->foreign('pavID')->references('id')->on('paveiksleliai');
        });
		
		 Schema::create('fzjungtis', function (Blueprint $table) {
            $table->integer('zanrID')->nullable()->unsigned();
            $table->integer('filmID')->nullable()->unsigned();
			$table->primary(['zanrID', 'filmID']);
			$table->foreign('filmID')->references('id')->on('filmai');
			$table->foreign('zanrID')->references('id')->on('zanrai');
        });
		 Schema::create('fajungtis', function (Blueprint $table) {
            $table->integer('aktorID')->nullable()->unsigned();
            $table->integer('filmID')->nullable()->unsigned();
			$table->primary(['aktorID', 'filmID']);
			$table->foreign('filmID')->references('id')->on('filmai');
			$table->foreign('aktorID')->references('id')->on('aktoriai');
        });
		 Schema::create('frjungtis', function (Blueprint $table) {
            $table->integer('reziID')->nullable()->unsigned();
            $table->integer('filmID')->nullable()->unsigned();
			$table->primary(['reziID', 'filmID']);
			$table->foreign('filmID')->references('id')->on('filmai');
			$table->foreign('reziID')->references('id')->on('rezisieriai');
        });
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagrindas');
    }
}
